/* Extend atof to handle scientific notation of the form 123.45e-6 where a
 * float-point number may be followed by e or E and an optionally signed exponent */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

double atof (char *s);
int power10 (int);

int main (void)
{
    char *s = "12.0E+2";
    puts(s);
    printf("atof: %e\n", atof(s));

    return 0;

}

/* atof : convert string s to double data type */
double atof (char *s)
{
    double val, power, power_e = 0.0;
    int i, sign;char temp;

    
    /*  skip white space */
    for (i = 0; isspace(s[i]); ++i)
    {
        /*  do nothing */
    }

    sign = (s[i] == '-') ? (-1) : 1;
    if ((s[i] == '-') || (s[i] == '+'))
    {
        ++i;
    }
    for (val = 0.0; isdigit(s[i]); ++i)
    {
        val = 10.0 * val + (s[i] - '0');
    }
    if (s[i] == '.')
    {
        ++i;
    }
    for (power = 1.0; isdigit(s[i]); ++i)
    {
        val = 10.0 * val + (s[i] - '0');
        power *= 10.0;
    }

    if ((s[i] == 'e' || s[i] == 'E') && ((s[i + 1] == '-') || (s[i + 1] == '+')))
    {
        temp = s[i + 1];
        i += 2;
        for (; isdigit(s[i]); ++i)
        {
            power_e = power_e * 10.0 + (s[i] - '0');
        }
    }
    if (temp == '-')
    {
        return (sign * val * 1.0) / (power * 1.0 * power10(power_e));
    }
    else if (temp == '+')
    {
        return (sign * val * 1.0 * power10(power_e)) / power;
    }
    else
    {
        return sign * val / power;
    }
}

int power10 (int x)
{
    int result = 1;
    for (int i = 0; i < x; ++i)
    {
        result *= 10;
    }

    return result;
}
