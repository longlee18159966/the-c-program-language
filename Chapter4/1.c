/* Write the function strindex_rightmost(s, t), which returns 
   the position of the right most occurence of t in s,
   or -1 if there is none */
   
#include <stdio.h>
#include <string.h>
   
int strindex_rightmost (char *s, char *t);
int strindex_leftmost (char *s, char *t);

int main (void)
{
    char *s = "le ngoc long le ngoc long";
    char *t = "on";
    printf("%d\n", strindex_rightmost(s, t));
    printf("%d", strindex_leftmost(s, t));
    
    return 0;
}

int strindex_rightmost (char *s, char *t)
{
    int i, j, k;
    for ( i = strlen(s) - 1; i >= strlen(t) - 1; i--)
    {
        int test = 1;
        for (j = i, k = strlen(t) - 1; k >= 0; j--, k--)
        {
            if (s[j] != t[k])
            {
                test = 0;
                break;
            }
        }
        if (!test)
        {
            continue;
        }
        else
        {
            return i - 1;
        }
        
    }

    return -1;
}

int strindex_leftmost (char *s, char *t)
{
    int i, j, k;
    
    for (i = 0; s[i] != '\0'; ++i)
    {
        for (j = i, k = 0; t[k] != '\0', s[j] == t[k]; ++j, ++k)
        {
            /* do nothing */
        }
        if (k > 0 && t[k] == '\0')
        {
            return i;
        }
    }
    
    return -1;
}
