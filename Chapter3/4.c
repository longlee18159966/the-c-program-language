/* In a two's complement number representation, our version of itoa does not
   handle the largest negative number, that is, the value of n equal to 
   -(2^(wordsize-1)).Explain why not? Modify it to print that value correctly
   regardless of the machine on which it runs. */
   
   /* explain: because the largest positive is (2^(wordsize-1) - 1) so our version
   of itoa does not handle the larget negative */
   
#include <stdio.h>
#include <string.h>

#define abs(x) (((x) < 0) ? (-(x)) : (x))

void itoa (int, char *);
void reverse (char *);

int main(void)
{
    char s[100];
    itoa(12367, s);
    puts(s);
    
    return 0;
}

void reverse (char *s)
{
    int c, i ,j;
    for (i = 0, j = strlen(s) - 1; i < j; ++i, --j)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    
    return ;
}

void itoa (int n, char *s)
{
    int i, sign;
    i = 0;
    sign = n;
    do
    {
        s[i++] = abs(n % 10) + '0';
    }
    while ((n /= 10) > 0);
    
    if (sign < 0)
    {
        s[i++] = '-';
    }
    else
    {
        s[i++] = '+';
    }
    
    s[i] = '\0';
    reverse(s);
    
    return ;
}

