/* Write the function itob(n, s, b) that converts the integer 
   into a base b character representation in the string s.
   In particular, itob(n, s, 16) formats n as a hexadecimal
   integer in s */
   
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define abs(x)  ((x < 0) ? ((-x)) : (x))

void itob (int n, char *s, int b);
void reverse (char *s);

int main(void)
{
     char s[100];
    itob(12367, s, 16);
    puts(s);
    
    return 0;
}

void itob (int n, char *s, int b)
{
    int i, sign;
    sign = n;
    i = 0;
    if (16 == b)
    {
        do
        {
            int temp = abs(n % b);
            if (temp >= 10)
            {
                s[i++] = temp + '7';
            }
            else
            {
                s[i++] = temp + '0';
            }
        }
        while ((n /= b) > 0);
    }
    else
    {
        do
        {
            s[i++] = abs(n % 10) + '0';
        }
        while ((n /= b) > 0);
    }
    
    if (sign < 0)
    {
        s[i++] = '-';
    }
    else
    {
        s[i++] = '+';
    }
    
    s[i] = '\0';
    reverse(s);
    
    return ;
}

void reverse (char *s)
{
    int c, i ,j;
    for (i = 0, j = strlen(s) - 1; i < j; ++i, --j)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    
    return ;
}
