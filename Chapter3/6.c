/* Write a version of itoa that accepts three arguments instead of two.
   The third argument is a munimum field width; the converted number
   must be padded with blanks on the left if necessary to make it wide enough. */
   
#include <stdio.h>
#include <string.h>

#define abs(x) (((x) < 0) ? (-(x)) : (x))

void itoa (int, char *, int);
void reverse (char *);

int main(void)
{
    char s[100];
    itoa(12367, s, 6);
    puts(s);
    
    return 0;
}

void reverse (char *s)
{
    int c, i ,j;
    for (i = 0, j = strlen(s) - 1; i < j; ++i, --j)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    
    return ;
}

void itoa (int n, char *s, int a)
{
    int i, sign;
    i = 0;
    sign = n;
    do
    {
        s[i++] = abs(n % 10) + '0';
    }
    while ((n /= 10) > 0);
   
    if (sign < 0)
    {
        s[i++] = '-';
    }
    else
    {
        s[i++] = '+';
    }
     
    while (i < a)
    {
        s[i++] = ' ';
    }
    
    s[i] = '\0';
    reverse(s);
    
    return ;
}

