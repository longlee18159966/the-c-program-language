/* Our binary search makes two tests inside the loop,
   when one would suffice(at the price of more test outside)
   Write a version with only one test inside the loop and measure
   the difference in run-time */
   
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define HIGH    9999
#define LOW     0
int binsearch_book (int, int [], int);
int binsearch_me (int, int [], int);

int main(void)
{
    int s1[HIGH];
    int s2[HIGH];
    int i;
    clock_t star1, star2, end1, end2;
    double time1, time2;

    srand((unsigned int) time(0));

    for ( i = LOW; i < HIGH; ++i)
    {
        s1[i] = rand();
    }

    srand((unsigned int) time(0));
     for ( i = LOW; i < HIGH; ++i)
    {
        s2[i] = rand();
    }
    s1[300] = s2[300] = 23;
    star1   = clock();
    binsearch_me(23, s1, HIGH);
    end1    = clock();
    star2   = clock();
    binsearch_book(23, s2, HIGH);
    end2    = clock();
    time1   = (double) (end1 - star1) / CLOCKS_PER_SEC;
    time2   = (double) (end2 - star2) / CLOCKS_PER_SEC;

    printf("me time: %lf ms\n", time1 * 1000);
    printf("book time: %lf ms\n", time2 * 1000);
    
    return 0;
}

int binsearch_me (int x, int v[], int n)
{
    int low, high, mid;
    low     = 0;
    high    = n - 1;
    while (low <= high)
    {
        if (v[mid] == x)
            return mid;
        else
        {
            if (x < v[mid])
                high = mid - 1;
            else
                low  = mid + 1;
        }
    }
    
    return -1;
}

int binsearch_book (int x, int v[], int n)
{
    int low, high, mid;
    low     = 0;
    high    = n - 1;
    while (low <= high)
    {
        mid = (low + high) / 2;
        if (x < v[mid])
            high = mid - 1;
        else if (x > v[mid])
            low  = mid + 1;
        else
            return mid;
    }
    
    return -1;
}
