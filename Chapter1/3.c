#include <stdio.h>
#define STEP 20
#define UP   300
#define DOWN 0

int main(void)
{
	float F = 0;
	printf("Fahrenheit Celsius\n");
	for (F = DOWN; F < UP; F += STEP)
	{
		printf("%6.1f %9.1f\n", F, (5.0/9.0)*(F-32));
	}

	return 0;
}
