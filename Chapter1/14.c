#include <stdio.h>

#define SIMILAR 1
#define UNSIMILAR 0

#define LENGTH 20

int main(void)
{
	int c, count_diff, state;
	int x = 0;
	int character_diff[LENGTH];
	int num_character_diff[LENGTH];
	count_diff = 0;
	state = SIMILAR;

	for (int index = 0; index < LENGTH; index++)
	{
		num_character_diff[index] = character_diff[index] = 0;
	}

	while ((c = getchar()) != EOF)
	{
		if (x == 0)
		{
			character_diff[0] = c;
			x = 1;
			count_diff = 1;
		}
		for (int index = 0; index < count_diff; index++)
		{
			if (c == character_diff[index])
			{
				num_character_diff[index]++;
				state = SIMILAR;
				break;
			}
			else
			{
				state = UNSIMILAR;
			}
		}
		
		if (state == UNSIMILAR)
		{
			character_diff[count_diff] = c;
			num_character_diff[count_diff]++;
			count_diff++;
		}
	}
	printf("%d\n", count_diff);
	putchar('\n');
	for (int index = 0; index < count_diff; index++)
	{
		if (character_diff[index] == '\n')
		{
			printf("Character: \\n frequency: %d\n", num_character_diff[index]);
		}
		else if (character_diff[index] == '\t')
		{
			printf("Character: \\t frequency: %d\n", num_character_diff[index]);
		}
		else
		{
			printf("Character: %c frequency: %d\n", character_diff[index], num_character_diff[index]);
		}
	}

	return 0;
}
