#include <stdio.h>

#define MAXLINE 1000

int getstrings (char line[], int leng);
void reverses_string (char line[], int leng);

int main(void)
{
    char string_test[MAXLINE];
    int x = 0;
    x = getstrings(string_test, MAXLINE);
    printf("%d\n", x);
    printf("%s\n", string_test);
    reverses_string(string_test, x);
    printf("%s\n", string_test);

    return 0;

}

int getstrings(char line[], int leng)
{
    int c, len;
    len = 0;

    while (((c = getchar()) != EOF) && (len < leng - 1) && (c != '\n'))
    {
        line[len] = c;
        ++len;
    }

    if (c == '\n')
    {
        line[len] = '\0';
    }

    return len;
}

void reverses_string (char line[], int leng)
{
    char temp = 0;

    for (int index = 0; index < (leng / 2); ++index)
    {
        temp = line[index];
        line[index] = line[leng - 1 - index];
        line[leng - 1 - index] = temp;
    }

    return ;
}