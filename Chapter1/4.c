#include <stdio.h>
#define STEP 1
#define UP   10
#define DOWN 0

int main(void)
{
	float C = 0;
	printf("Celsius   Fahrenheit\n");
	for (C = DOWN; C < UP; C += STEP)
	{
		printf("%6.1f %9.1f\n", C, (9.0/5.0)*C + 32);
	}

	return 0;
}
