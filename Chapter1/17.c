#include <stdio.h>

#define MAXLINE 1000
#define MAXCHAR 80
#define MAXNUM 10

void coppy_string(char from[],char to[]);
int getlines(char line[], int maxlines);

int main(void)
{
	int length;
	int maxlength;
	int count = 0;
	char line[MAXLINE];
	char print_line80[MAXNUM][MAXLINE];

	while ((length = getlines(line, MAXLINE)) > 0)
	{
		if (length > MAXCHAR)
		{
			coppy_string(line, print_line80[count++]);
		}
	}
	printf("%d\n", count);
	for (int index = 0; index < count; index++)
	{
		printf("%s\n", print_line80[index]);
	}

	return 0;
}

void coppy_string(char from[], char to[])
{
	int i = 0;
	while ((to[i] = from[i]) != '\0')
	{
		i++;
	}

	return ;
}

int getlines(char line[], int maxlines)
{
	int c, i;
	for (i = 0; (i < maxlines-1) && ((c = getchar()) != EOF) && (c != '\n'); i++)
	{
		line[i] = c;
	}
	if (c == '\n')
	{
		line[i++] = c;
	}

	line[i] = '\0';

	return i;
}
