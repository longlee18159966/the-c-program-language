#include <stdio.h>

#define IN 1	/*in words*/
#define OUT 0	/*out words*/

/*count line, word, character*/

int main(void)
{
	int c, state;
	state = OUT;
	while ((c = getchar()) != EOF)
	{
		if ((c == ' ') || (c == '\t') || (c == '\n'))
		{
			state = OUT;
		}
		if (state == OUT)
		{
			state = IN;
			putchar('\n');
		}
		if ((state == IN) && (c != ' ') && (c != '\t') && (c != '\n'))
		{
			putchar(c);
		}
	}

	return 0;
}
