#include <stdio.h>

int main(void)
{
	int c, tab, newline, blank;

	tab = newline = blank = 0;
	while ((c = getchar()) != EOF)
	{
		if (c == '\t')
		{
			tab++;
		}
		if (c == '\n')
		{
			newline++;
		}
		if (c == ' ')
		{
			blank++;
		}
		
	}
	printf("\nblank: %d\ntab: %d\nnewline: %d\n", blank, tab, newline);

	return 0;
}
