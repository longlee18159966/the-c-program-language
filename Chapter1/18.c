#include <stdio.h>

#define MAXLINE 1000
#define MAXNUM 10

#define TRUE 1
#define FALSE 0

void coppy_string(char from[],char to[]);
int getlines(char line[], int maxlines);

int main(void)
{
	int length;
	int maxlength;
	int count = 0;
	char line[MAXLINE];
	char remove_trailing[MAXNUM][MAXLINE];

	while ((length = getlines(line, MAXLINE)) > 0)
	{
		for (int index = 0; index < length; index++)
		{
			if ((line[index] == ' ') || (line[index] == '\t'))
			{
				for (int i = index; i < length; i++)
				{
					line[i] = line[i+1];
				}
				index--;
			}
		}
		coppy_string(line, remove_trailing[count++]);
	}

	for (int index = 0; index < count; index++)
	{
		int test = FALSE;
		for (int i = 0; remove_trailing[index][i] != '\0'; i++)
		{
			if ((remove_trailing[index][i] != '\t') && (remove_trailing[index][i] != ' ') && (remove_trailing[index][i] != '\n'))
			{
				test = TRUE;
			}
		}
		if (test == TRUE)
		{
			printf("%s", remove_trailing[index]);
		}
	}

	return 0;
}

void coppy_string(char from[], char to[])
{
	int i = 0;
	while ((to[i] = from[i]) != '\0')
	{
		i++;
	}

	return ;
}

int getlines(char line[], int maxlines)
{
	int c, i;
	for (i = 0; (i < maxlines-1) && ((c = getchar()) != EOF) && (c != '\n'); i++)
	{
		line[i] = c;
	}
	if (c == '\n')
	{
		line[i++] = c;
	}

	line[i] = '\0';

	return i;
}
