#include <stdio.h>

#define OUT 0	/*out word*/
#define IN 1	/*in word*/
#define LENGTH	30

int main(void)
{
	int c, state, count = 0;
	int length_word[LENGTH];
	state = OUT;
	for (int index = 0; index < LENGTH; index++)
	{
		length_word[index] = 0;
	}

	while ((c = getchar()) != EOF)
	{
		if ((c == ' ') || (c == '\n') || (c == '\t'))
		{
			state = OUT;
		}
		if (state == OUT)
		{
			if (count > 0)
			{
				length_word[count-1]++;
			}
			state = IN;
			count = 0;
		}
		if ((state == IN) && (c != ' ') && (c != '\n') && (c != '\t'))
		{
			count++;
		}
	}
	printf("\nlength of word:");
	for (int index1 = 1; index1 <= LENGTH; index1++)
	{
		printf("%6d character", index1);
		if ((index1 % 10) == 0)
		{
			printf("\nnumber of word:");
			for (int index2 = index1 - 10; index2 < index1; index2++)
			{
				printf("%6d          ", length_word[index2]);
				if (((index2 % 10) == 9) && (index2 < LENGTH - 1))
				{
					printf("\nlength of word:");
				}
			}
		}
	}
	putchar('\n');

	return 0;
}
