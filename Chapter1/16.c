#include <stdio.h>

#define MAXLINE 1000

void coppy_string(char from[],char to[]);
int getlines(char line[], int maxlines);

int main(void)
{
	int length;
	int maxlength;
	char line[MAXLINE];

	while ((length = getlines(line, MAXLINE)) > 0)
	{
		if (length > 0)
		{
			printf("%s length: %d\n", line, length);
		}
	}

	return 0;
}

void coppy_string(char from[], char to[])
{
	int i = 0;
	while ((to[i] = from[i]) != '\0')
	{
		i++;
	}

	return ;
}

int getlines(char line[], int maxlines)
{
	int c, i;
	for (i = 0; (i < maxlines-1) && ((c = getchar()) != EOF) && (c != '\n'); i++)
	{
		line[i] = c;
	}
	if (c == '\n')
	{
		line[i++] = c;
	}

	line[i] = '\0';

	return i;
}
