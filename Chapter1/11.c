#include <stdio.h>

#define IN 1	/*in words*/
#define OUT 0	/*out words*/

/*count line, word, character*/

int main(void)
{
	int c, line, word, character, state;
	word = character = 0;
	state = OUT;
	line = 1;
	while ((c = getchar()) != EOF)
	{
		if (c == '\n')
		{
			line++;
		}
		if ((c == ' ') || (c == '\t') || (c == '\n'))
		{
			state = OUT;
		}
		if (state == OUT)
		{
			word++;
			state = IN;
		}
		character++;
	}

	printf("\nline %d\nword: %d\ncharacter: %d\n", line, word, character);

	return 0;
}
