/* detab progrram */
#include <stdio.h>

#define MAXLINE 1000
#define N_TAB   4

int main(void)
{
    char c;
    char arr_char[MAXLINE];
    int count = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == '\t')
        {
            for (int index = 0; index < N_TAB; ++index)
            {
                arr_char[count] = ' ';
                ++count;
            }
        }
        else
        {
            arr_char[count] = c;
            ++count;
        }
    }

    printf("%s\n", arr_char);

    return 0;
}