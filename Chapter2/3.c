/* Write the function htoi(s), which converts a string
 * of hexa-decimal digits (including an optional 0x or 0X)
 * into its equivalent integer value.The allowable digits
 * are 0 through 9, a through f, and A through F */

#include <stdio.h>

#define LENGTH 20
int htoi (char *);
void get_hexa (char *);

int main(void)
{
    char hexal[LENGTH];
    get_hexa(hexal);
    printf("%d\n", htoi(hexal));

    return 0;
}



/* htoi : converts a string of hexa-decimal digits into its
 * equavilent integer value */
int htoi (char *s)
{
    int count = 0;
    int value = 0;
    int hex   = 0;
    if (('0' == s[count]) && (('x' == s[count+1]) || ('X' == s[count+1])))
    {
        count = 2;
    }

    for (; s[count] != '\0'; count++)
    {
       hex = 0;
        if ((s[count] >= '0') && (s[count] <= '9'))
        {
            hex += (s[count] - '0');
        }
        else if ((s[count] >= 'a') && (s[count] <= 'f'))
        {
            hex += (s[count] - 'a' + 10);
        }
        else if ((s[count] >= 'A') && (s[count] <= 'F'))
        {
            hex += (s[count] - 'A' + 10);
        }
        else
        {
            printf("illegal value\nEnd\n");
            return 0;
        }
        value = 16 * value + hex;
    }

    return value;
}

/* get_string : get hexa-decimal from input */
void get_hexa (char *s)
{
    printf("Please enter hexa-decimal\n");
    scanf("%s", s);

    return ;
}
