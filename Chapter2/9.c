/* In a two's complement number system, x &= (x - 1) deletes the rightmost
  1-bit in x. Explain why?Use this observation to write a faster version
  of bitcount */

#include <stdio.h>

/* bitcount : count the number of 1-bits in its integer argument */
int bitcount1 (unsigned x);
int bitcount2 (unsigned x);

int main(void)
{
  int x = 134;
  printf("bitcount1: %d, bitcount2: %d", bitcount1(x), bitcount2(x)); 

  return 0;
}

int bitcount2 (unsigned x)
{
  int count = 0;
  for (count = 0; x != 0; x >>= 1)
    {
      if (x & 01)
        count++;
    }

  return count;
}

int bitcount1 (unsigned x)
{
  int count = 0;
  while (0 != x)
    {
      x &= (x - 1);
      ++count;
    }

  return count;
}
