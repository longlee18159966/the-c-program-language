/* Write a function invert(x, p, n); that return x with the n bits that begin at position p inverted (i.e., 1 changed into 0 and vice versa), leaving the others unchanged */

#include <stdio.h>

unsigned char invert (unsigned char, unsigned char, unsigned char);

int main() {
    // Write C code here
    printf("%u\n", invert(167, 5, 3));
    
    return 0;
}

unsigned char invert (unsigned char x, unsigned char p, unsigned char n)
{
     return x ^ (~(~0 << n) << (p + 1 - n));
}
