/* Rewrite the function lower, which converts upper case letters to lower
   case, with a conditional expression instead of if-else */

#include <stdio.h>

char lower (char);

int main(void)
{
  char x = 'A';
  printf("%c", lower(x));
}

char lower (char x)
{
  return (x >= 'A' && x <= 'Z') ? x + 32 : x;
}
