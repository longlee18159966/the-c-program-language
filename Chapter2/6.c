/* Write a function setbits(x, p, n, y) that returns x with the n bits
 * that begin at position p set to the rightmost n bits of y, leaving the 
 * other bits unchanged */

#include <stdio.h>

unsigned setbits (unsigned x, unsigned p, unsigned n, unsigned y);

int main (void)
{
    printf("%u\n", setbits(209, 6, 3, 108));

    return 0;
}
unsigned setbits (unsigned x, unsigned p, unsigned n, unsigned y)
{
    unsigned val = 0;
    for (int i = 0; i < n; ++i)
    {
        val ^= (unsigned) (1 << (6 - i));
    }

    y &= val;
    x &= (~val);
    x |= y;

    return x;
}
