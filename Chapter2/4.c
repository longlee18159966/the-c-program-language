/* Write an alternate version of squeeze(s1, s2) that deletes each character in s1
 * that matches any character in the string s2. */

#include <stdio.h>

#define LENGTH    100

void squeeze (char s1[], const char s2[]);
void get_string (char s1[], char s2[]);
int main (void)
{
    char s1[LENGTH];
    char s2[LENGTH];
    get_string(s1, s2);
    squeeze(s1, s2);
    puts(s1);

    return 0;
}

void get_string (char s1[], char s2[])
{
    printf("Please enter string 1:\n");
    fgets(s1, LENGTH, stdin);
    printf("Please enter string 2:\n");
    fgets(s2, LENGTH, stdin);
    return ;
}

void squeeze (char s1[], const char s2[])
{
    char temp_val;
    int a, b;
    for (int i = 0; s2[i] != '\0'; ++i)
    {
        if (temp_val != s2[i])
        {
            temp_val = s2[i];
        }
        else
        {
            continue;
        }
        for ( a = b = 0; s1[a] != '\0'; ++a)
        {
            if (s1[a] != temp_val)
            {
                s1[b++] = s1[a];
            }
        }
        s1[b] = '\0';
    }

    return ;
}


