/* Write the function any(s1, s2), which returns thr first location in the string s1
 * where any character from the string s2 occurs, or -1 if s1 contains no characters 
 * from s2.(The standard library function strpbrk does the same job but return a
 * pointer to the location */

#include <stdio.h>

#define LENGTH    100

int any (const char *s1, const char *s2);
void get_string (char s1[], char s2[]);

int main (void)
{
    char s1[LENGTH];
    char s2[LENGTH];
    get_string(s1, s2);
    printf("%d\n", any(s1, s2));

    return 0;
}

void get_string (char s1[], char s2[])
{
    printf("Please enter string 1:\n");
    fgets(s1, LENGTH, stdin);
    printf("Please enter string 2:\n");
    fgets(s2, LENGTH, stdin);

    return ;
}

int any (const char *s1, const char *s2)
{
    char temp_val;
    for (int i = 0; s2[i] != '\0'; ++i)
    {
        if (temp_val != s2[i])
        {
            temp_val = s2[i];
        }
        for (int a = 0; s1[a] != '\0'; ++a)
        {
            if (temp_val == s1[a])
            {
                return a + 1;
            }
        }
    }

    return -1;
}
